# React-Native

Projetos para estudar o react-native

***Config Ambient***
1.  install sdk android
2.  create variable ambient `ANDROID_HOME`
3.  add to path `%ANDROID_HOME%/{path}`
4.  run power shell `.\sdkmanager.bat  "platform-tools" "platforms;android-27" "build-tools;27.0.3" --sdk_root= ${ANDROID_HOME}`
5.  run `.\sdkmanager.bat --licenses --sdk_root= ${ANDROID_HOME}`
6.  run `npm i react-native -g`  

***Comands***
*  `react-native run-android`